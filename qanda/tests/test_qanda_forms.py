# -*- encoding: utf-8 -*-
from qanda.forms import AnswerForm, QuestionForm
from qanda.tests.qanda_cases import TestQandACase


class TestQandAPerm(TestQandACase):
    def test_qanda_form_ask_question(self):
        data = {
            "user": self.u1.pk,
            "category": self.cat2.pk,
            "question": "Did this test work?",
        }
        form = QuestionForm(data=data)
        print(form.errors)
        self.assertTrue(form.is_valid())

    def test_qanda_form_answer_question(self):
        data = {
            "pk": self.q3.pk,
            "user": self.u1.pk,
            "category": self.cat1.pk,
            "question": self.q3.question,
            "mentor": self.u2.pk,
            "answer": "I really hope so.",
            "status": 2,
        }
        form = AnswerForm(data=data)
        self.assertTrue(form.is_valid())
