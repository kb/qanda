# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse

from login.tests.fixture import perm_check
from qanda.tests.factories import QandAFactory  # , QandAMentorFactory
from qanda.tests.qanda_cases import TestQandACase


@pytest.mark.django_db
def test_answer_update(perm_check):
    question = QandAFactory()
    url = reverse("qanda.answer", args=[question.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_list(perm_check):
    QandAFactory()
    url = reverse("qanda.list")
    perm_check.staff(url)


@pytest.mark.django_db
def test_list_unanswered(perm_check):
    QandAFactory(answer="")
    url = reverse("qanda.list.unanswered")
    perm_check.staff(url)


@pytest.mark.django_db
def test_mark_complete_update(perm_check):
    question = QandAFactory()
    url = reverse("qanda.mark.complete", args=[question.pk])
    perm_check.staff(url)


# @pytest.mark.django_db
# def test_mentor_create(perm_check):
#     url = reverse("qanda.mentor.create")
#     perm_check.staff(url)
#
#
# @pytest.mark.django_db
# def test_mentor_delete(perm_check):
#     x = QandAMentorFactory(email="patrick@pkimber.net")
#     url = reverse("qanda.mentor.delete", args=[x.pk])
#     perm_check.staff(url)
#
#
# @pytest.mark.django_db
# def test_mentor_list(perm_check):
#     url = reverse("qanda.mentor.list")
#     perm_check.staff(url)
