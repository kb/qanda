# -*- encoding: utf-8 -*-
from qanda.models import QandA
from qanda.tests.qanda_cases import TestQandACase


class TestCaseTestQandACase(TestQandACase):
    def test_qanda_cases_testqandacase_users(self):
        self.assertEqual(self.cat1.name, "general")
        self.assertEqual(self.cat2.name, "other")
        self.assertEqual(self.u1.username, "fred")
        self.assertEqual(self.u2.username, "mike")
        self.assertEqual(self.u3.username, "sara")

    def test_qanda_cases_testqandacase_questions(self):
        self.assertEqual(self.q1.question, "What is blue?")
        self.assertEqual(self.q1.status, QandA.QANDA_STATE_CLAIMED)
        self.assertEqual(self.q2.status, QandA.QANDA_STATE_PUBLISHED)
        self.assertEqual(self.q3.status, QandA.QANDA_STATE_ASKED)
        self.assertEqual(QandA.objects.count(), 6)
        self.assertEqual(QandA.objects.published().count(), 1)

    def test_qanda_cases_testqandacase_addanswer(self):
        self.assertEqual(QandA.objects.published().count(), 1)
        self.addanswer()
        self.assertEqual(QandA.objects.published().count(), 2)
