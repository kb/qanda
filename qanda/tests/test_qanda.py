# -*- encoding: utf-8 -*-
import pytest

from login.tests.factories import UserFactory
from qanda.models import QandA
from qanda.tests.factories import QandAFactory, QandACategoryFactory


@pytest.mark.django_db
def test_answer_question():
    mentor = UserFactory()
    x = QandA.objects.ask(UserFactory(), "Colour?", QandACategoryFactory())
    x.answer_question(mentor, "Green", QandA.QANDA_STATE_PUBLISHED)
    x.refresh_from_db()
    assert "Green" == x.answer
    assert mentor == x.mentor
    assert QandA.QANDA_STATE_PUBLISHED == x.status


@pytest.mark.django_db
def test_ask():
    user = UserFactory()
    category = QandACategoryFactory()
    x = QandA.objects.ask(user, "What is your favourite colour?", category)
    assert "What is your favourite colour?" == x.question
    assert user == x.user
    assert category == x.category


@pytest.mark.django_db
def test_ask_no_category():
    user = UserFactory()
    x = QandA.objects.ask(user, "What is your favourite colour?")
    assert "What is your favourite colour?" == x.question
    assert user == x.user
    assert x.category is None


@pytest.mark.django_db
@pytest.mark.parametrize(
    "status,answer,expect",
    [
        (QandA.QANDA_STATE_ASKED, "", "Review"),
        (QandA.QANDA_STATE_CLAIMED, "", "Review"),
        (QandA.QANDA_STATE_CLAIMED, "abc", "Draft"),
        (QandA.QANDA_STATE_MARK_COMPLETE, "", "Completed"),
        (QandA.QANDA_STATE_PUBLISHED, "", "Published"),
    ],
)
def test_button_caption(status, answer, expect):
    x = QandAFactory(answer=answer, status=status)
    assert expect == x.button_caption()


@pytest.mark.django_db
@pytest.mark.parametrize(
    "status,expect",
    [
        (QandA.QANDA_STATE_ASKED, True),
        (QandA.QANDA_STATE_CLAIMED, True),
        (QandA.QANDA_STATE_MARK_COMPLETE, False),
        (QandA.QANDA_STATE_PUBLISHED, False),
    ],
)
def test_can_mark_complete(status, expect):
    x = QandAFactory(status=status)
    assert x.can_mark_complete() is expect


@pytest.mark.django_db
@pytest.mark.parametrize(
    "status,expect",
    [
        (QandA.QANDA_STATE_ASKED, False),
        (QandA.QANDA_STATE_CLAIMED, False),
        (QandA.QANDA_STATE_MARK_COMPLETE, True),
        (QandA.QANDA_STATE_PUBLISHED, False),
    ],
)
def test_is_completed(status, expect):
    x = QandAFactory(status=status)
    assert x.is_completed() is expect
