# -*- encoding: utf-8 -*-
import pytest

from django.core.management import call_command


@pytest.mark.django_db
def test_qanda_init_app():
    """Test the management command"""
    call_command("init_app_qanda")
