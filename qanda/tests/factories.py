# -*- encoding: utf-8 -*-
import factory
import string

from django.conf import settings

# https://github.com/rbarrois/factory_boy/issues/138
# from factory import fuzzy

from login.tests.factories import UserFactory
from qanda.models import QandA  # , QandAMentor

Q_PREFIXES = ["How", "What", "Why", "When", "Can"]


class QandACategoryFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = settings.QANDA_CATEGORY_MODEL

    @factory.sequence
    def name(n):
        return "name_{:02d}".format(n)


# class QFactory(factory.django.DjangoModelFactory):
#     """Special factory."""
#
#     class Meta:
#         model = QandA
#
#     user = factory.SubFactory(UserFactory)
#     category = factory.SubFactory(QandACategoryFactory)


# class QandAMentorFactory(factory.django.DjangoModelFactory):
#     class Meta:
#         model = QandAMentor


class QandAFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = QandA

    user = factory.SubFactory(UserFactory)
    category = factory.SubFactory(QandACategoryFactory)
    # mentor = factory.SubFactory(UserFactory)

    @factory.sequence
    def answer(n):
        return "answer_{:02d}".format(n)

    @factory.sequence
    def question(n):
        return "question_{:02d}".format(n)
