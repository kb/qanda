# -*- encoding: utf-8 -*-
import pytest

from qanda.models import QandA
from qanda.tests.factories import QandAFactory
from qanda.tests.qanda_cases import TestQandACase


@pytest.mark.django_db
def test_needing_answers():
    QandAFactory(status=QandA.QANDA_STATE_ASKED)
    QandAFactory(status=QandA.QANDA_STATE_CLAIMED)
    QandAFactory(status=QandA.QANDA_STATE_PUBLISHED)
    QandAFactory(status=QandA.QANDA_STATE_MARK_COMPLETE)
    assert [QandA.QANDA_STATE_ASKED, QandA.QANDA_STATE_CLAIMED] == [
        x.status for x in QandA.objects.needing_answers().order_by("status")
    ]


class TestQandAManager(TestQandACase):
    def test_qanda_manager_questions_and_answers(self):
        self.assertEqual(QandA.objects.questions_and_answers().count(), 6)
        # Asked 6
        self.assertEqual(
            QandA.objects.questions_and_answers(category=self.cat2).count(), 2
        )
        self.assertEqual(
            QandA.objects.questions_and_answers(category=self.cat1).count(), 4
        )
        # Asked 2 "general and 4 self.cat1
        self.assertEqual(
            QandA.objects.questions_and_answers(user=self.u1).count(), 2
        )
        # tim Asked 1 + Answered 1
        self.assertEqual(
            QandA.objects.questions_and_answers(user=self.u2).count(), 3
        )
        # tom Asked 2 + Answered 1
        self.assertEqual(
            QandA.objects.questions_and_answers(user=self.u3).count(), 3
        )
        # tam Asked 3 + Answered 0
        self.assertEqual(
            QandA.objects.questions_and_answers(
                user=self.u1, category=self.cat2
            ).count(),
            1,
        )
        self.assertEqual(
            QandA.objects.questions_and_answers(
                user=self.u1, category=self.cat1
            ).count(),
            1,
        )
        # tim Asked 1 self.cat1 + Answered 1 self.cat2
        self.assertEqual(
            QandA.objects.questions_and_answers(
                user=self.u2, category=self.cat2
            ).count(),
            1,
        )
        self.assertEqual(
            QandA.objects.questions_and_answers(
                user=self.u2, category=self.cat1
            ).count(),
            2,
        )
        # tom Asked 1 of each + Answered 1 self.cat1
        self.assertEqual(
            QandA.objects.questions_and_answers(
                user=self.u3, category=self.cat2
            ).count(),
            1,
        )
        self.assertEqual(
            QandA.objects.questions_and_answers(
                user=self.u3, category=self.cat1
            ).count(),
            2,
        )
        # tam Asked 1 general, 2 other
        self.addanswer()
        # Action: answer one of the questions needing_answers.
        self.assertEqual(
            QandA.objects.questions_and_answers(
                user=self.u1, category=self.cat2
            ).count(),
            1,
        )
        self.assertEqual(
            QandA.objects.questions_and_answers(
                user=self.u1, category=self.cat1
            ).count(),
            2,
        )
        # NOW tim Answered tom's self.cat1

    def test_qanda_manager_needing_answers(self):
        self.assertEqual(QandA.objects.needing_answers().count(), 5)
        # 4 not answered
        self.assertEqual(
            QandA.objects.needing_answers(category=self.cat2).count(), 1
        )
        self.assertEqual(
            QandA.objects.needing_answers(category=self.cat1).count(), 4
        )
        # 1 self.cat2 and 3 self.cat1 need answers
        self.assertEqual(QandA.objects.needing_answers(user=self.u1).count(), 1)
        # tim None needing answers
        self.assertEqual(QandA.objects.needing_answers(user=self.u2).count(), 2)
        # tom 1 needing answers
        self.assertEqual(QandA.objects.needing_answers(user=self.u3).count(), 3)
        # tam 3 needing answers
        self.assertEqual(
            QandA.objects.needing_answers(
                user=self.u1, category=self.cat2
            ).count(),
            0,
        )
        self.assertEqual(
            QandA.objects.needing_answers(
                user=self.u1, category=self.cat1
            ).count(),
            1,
        )
        # tim Asked 1 self.cat1 + Answered 1 self.cat2
        self.assertEqual(
            QandA.objects.needing_answers(
                user=self.u2, category=self.cat2
            ).count(),
            0,
        )
        self.assertEqual(
            QandA.objects.needing_answers(
                user=self.u2, category=self.cat1
            ).count(),
            2,
        )
        # tom Asked 1 of each + Answered 1 self.cat1
        self.assertEqual(
            QandA.objects.needing_answers(
                user=self.u3, category=self.cat2
            ).count(),
            1,
        )
        self.assertEqual(
            QandA.objects.needing_answers(
                user=self.u3, category=self.cat1
            ).count(),
            2,
        )
        # tam needs answers 1 self.cat2 and 2 self.cat1
        self.addanswer()
        # Action: answer one of the questions needing_answers.
        self.assertEqual(QandA.objects.needing_answers(user=self.u2).count(), 1)
        self.assertEqual(
            QandA.objects.needing_answers(
                user=self.u2, category=self.cat2
            ).count(),
            0,
        )
        self.assertEqual(
            QandA.objects.needing_answers(
                user=self.u2, category=self.cat1
            ).count(),
            1,
        )
        # NOW tom has no questions needing_answers.

    def test_qanda_manager_published(self):
        self.assertEqual(QandA.objects.published().count(), 1)
        # 1 published
        self.assertEqual(QandA.objects.published(category=self.cat2).count(), 1)
        self.assertEqual(QandA.objects.published(category=self.cat1).count(), 0)
        # 1 self.cat2 published
        self.assertEqual(QandA.objects.published(user=self.u1).count(), 1)
        # tim 1 Asked published
        self.assertEqual(QandA.objects.published(user=self.u2).count(), 1)
        # tom 1 Answered published
        self.assertEqual(QandA.objects.published(user=self.u3).count(), 0)
        # tam 0 published Asked or Answered
        self.assertEqual(
            QandA.objects.published(user=self.u1, category=self.cat2).count(), 1
        )
        self.assertEqual(
            QandA.objects.published(user=self.u1, category=self.cat1).count(), 0
        )
        # tim 1 Asked published
        self.assertEqual(
            QandA.objects.published(user=self.u2, category=self.cat2).count(), 1
        )
        self.assertEqual(
            QandA.objects.published(user=self.u2, category=self.cat1).count(), 0
        )
        # tom 1 Answered published
        self.assertEqual(
            QandA.objects.published(user=self.u3, category=self.cat2).count(), 0
        )
        self.assertEqual(
            QandA.objects.published(user=self.u3, category=self.cat1).count(), 0
        )
        # tam 0 published Asked or Answered
        self.addanswer()
        # Action: answer one of the questions needing_answers.
        self.assertEqual(QandA.objects.published().count(), 2)
        # 2 published
        self.assertEqual(QandA.objects.published(category=self.cat2).count(), 1)
        self.assertEqual(QandA.objects.published(category=self.cat1).count(), 1)
        # 1 self.cat2 and 1 self.cat1 published
        self.assertEqual(
            QandA.objects.published(user=self.u1, category=self.cat2).count(), 1
        )
        self.assertEqual(
            QandA.objects.published(user=self.u1, category=self.cat1).count(), 1
        )
        # tim 1 Asked published and 1 Answered published
        self.assertEqual(
            QandA.objects.published(user=self.u2, category=self.cat2).count(), 1
        )
        self.assertEqual(
            QandA.objects.published(user=self.u2, category=self.cat1).count(), 1
        )
        # tom 1 Answered published and 1 Asked published
        self.assertEqual(QandA.objects.published(user=self.u3).count(), 0)
        # tam 0 published Asked or Answered
