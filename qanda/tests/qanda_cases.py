# -*- encoding: utf-8 -*-
"""qanda App Test Cases.

***Usage***::

    from qanda.tests.qanda_cases import TestQandACase"""
from base.tests.test_utils import PermTestCase
from login.tests.scenario import (
    default_scenario_login,
    get_user_fred,
    get_user_mike,
    get_user_sara,
    user_contractor,
)
from qanda.models import QandA, get_category_model


class TestQandACase(PermTestCase):
    def setUp(self):
        default_scenario_login()
        user_contractor()
        self.cat1 = get_category_model().objects.init_qanda_category(
            name="general"
        )
        self.cat2 = get_category_model().objects.init_qanda_category(
            name="other"
        )
        self.u1 = get_user_fred()
        self.u2 = get_user_mike()
        self.u3 = get_user_sara()
        self.q1 = QandA.objects.ask(self.u1, "What is blue?", self.cat1)
        self.q1.answer_question(
            self.u2, "Very little green and red.", QandA.QANDA_STATE_CLAIMED
        )
        self.q2 = QandA.objects.ask(self.u2, "What is green?", self.cat2)
        self.q2.answer_question(
            self.u1, "Very little blue and red.", QandA.QANDA_STATE_PUBLISHED
        )
        self.q3 = QandA.objects.ask(self.u2, "What is red?", self.cat1)
        self.q4 = QandA.objects.ask(self.u3, "What is purple?", self.cat1)
        self.q5 = QandA.objects.ask(self.u3, "What is yellow?", self.cat1)
        self.q6 = QandA.objects.ask(self.u3, "What is grey?", self.cat2)

    def addanswer(self):
        self.q3.answer_question(
            self.u1, "Very little blue and green.", QandA.QANDA_STATE_PUBLISHED
        )
