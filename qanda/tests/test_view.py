# -*- encoding: utf-8 -*-
import pytest

from datetime import date, datetime
from django.urls import reverse
from freezegun import freeze_time
from http import HTTPStatus

from login.tests.factories import TEST_PASSWORD, UserFactory
from mail.models import Message
from mail.tests.factories import MailTemplateFactory
from qanda.models import get_category_model, QandA
from qanda.tests.factories import QandAFactory


@pytest.mark.django_db
def test_answer_update_get(client):
    q_and_a = QandAFactory(answer="")
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("qanda.answer", args=[q_and_a.pk])
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code
    assert 1 == QandA.objects.count()
    q_and_a.refresh_from_db()
    assert "" == q_and_a.answer
    assert user == q_and_a.mentor
    assert QandA.QANDA_STATE_CLAIMED == q_and_a.status
    assert "warning" in response.context
    warning = response.context["warning"]
    assert warning is None


@pytest.mark.django_db
def test_answer_update_get_no_change_mentor(client):
    mentor = UserFactory(is_staff=True, first_name="P", last_name="Kimber")
    with freeze_time(datetime(2022, 1, 21, 10, 3)):
        q_and_a = QandAFactory(
            answer="", mentor=mentor, status=QandA.QANDA_STATE_CLAIMED
        )
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("qanda.answer", args=[q_and_a.pk])
    with freeze_time(datetime(2022, 3, 21, 10, 3)):
        response = client.get(url)
    assert HTTPStatus.OK == response.status_code
    assert 1 == QandA.objects.count()
    q_and_a.refresh_from_db()
    assert "" == q_and_a.answer
    assert mentor == q_and_a.mentor
    assert QandA.QANDA_STATE_CLAIMED == q_and_a.status
    assert "warning" in response.context
    warning = response.context["warning"]
    assert "Review: Question claimed by P Kimber (a month ago)" == warning


@pytest.mark.django_db
def test_mark_complete_update(client):
    mentor = UserFactory(is_staff=True, first_name="P", last_name="Kimber")
    # with freeze_time(datetime(2022, 1, 21, 10, 3)):
    q_and_a = QandAFactory(
        answer="", mentor=mentor, status=QandA.QANDA_STATE_CLAIMED
    )
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("qanda.mark.complete", args=[q_and_a.pk])
    with freeze_time(datetime(2022, 3, 21, 10, 3)):
        response = client.post(url, data={"reason": "Duplicate question..."})
    assert HTTPStatus.FOUND == response.status_code
    assert reverse("qanda.list.unanswered") == response.url
    assert 1 == QandA.objects.count()
    q_and_a.refresh_from_db()
    assert "" == q_and_a.answer
    assert user == q_and_a.mentor
    assert QandA.QANDA_STATE_MARK_COMPLETE == q_and_a.status
    assert "Duplicate question..." == q_and_a.reason
    assert 0 == Message.objects.count()


@pytest.mark.django_db
def test_mark_complete_update_no_reason(client):
    mentor = UserFactory(is_staff=True, first_name="P", last_name="Kimber")
    q_and_a = QandAFactory(
        answer="", mentor=mentor, status=QandA.QANDA_STATE_CLAIMED
    )
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("qanda.mark.complete", args=[q_and_a.pk])
    with freeze_time(datetime(2022, 3, 21, 10, 3)):
        response = client.post(url, data={})
    assert HTTPStatus.OK == response.status_code
    assert {"reason": ["You must enter a reason"]} == response.context[
        "form"
    ].errors
    assert 0 == Message.objects.count()


@pytest.mark.django_db
def test_answer_update_post_no_answer(client):
    q_and_a = QandAFactory(answer="")
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("qanda.answer", args=[q_and_a.pk])
    response = client.post(url, data={"answer": "   "})
    assert HTTPStatus.OK == response.status_code
    assert {"answer": ["You must enter an answer"]} == response.context[
        "form"
    ].errors


@pytest.mark.django_db
@pytest.mark.parametrize(
    "category_name,expect",
    [
        (None, "General Query Answer"),
        ("Farming", "Farming"),
    ],
)
def test_answer_update_post_publish(client, category_name, expect):
    MailTemplateFactory(slug=QandA.MAIL_TEMPLATE_QANDA_PUBLISH)
    qanda = QandAFactory(
        user=UserFactory(first_name="Patrick", last_name="Kimber"),
        question="Why would it?",
        answer="",
    )
    if category_name:
        category = get_category_model().objects.init_qanda_category(
            name=category_name
        )
        qanda.category = category
    else:
        qanda.category = None
    qanda.save()
    assert QandA.QANDA_STATE_ASKED == qanda.status
    assert 1 == QandA.objects.needing_answers().count()
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("qanda.answer", args=[qanda.pk])
    response = client.post(
        url,
        data={"answer": "Why would it not?", "publish": "makes-no-difference"},
    )
    assert HTTPStatus.FOUND == response.status_code
    assert reverse("qanda.list.unanswered") == response.url
    assert 0 == QandA.objects.needing_answers().count()
    qanda.refresh_from_db()
    assert "Why would it not?" == qanda.answer
    assert user == qanda.mentor
    assert QandA.QANDA_STATE_PUBLISHED == qanda.status
    # check email template context
    assert 1 == Message.objects.count()
    message = Message.objects.first()
    assert 1 == message.mail_set.count()
    mail = message.mail_set.first()
    assert 4 == mail.mailfield_set.count()
    result = {f.key: f.value for f in mail.mailfield_set.all()}
    # from rich.pretty import pprint
    # pprint(result, expand_all=True)
    assert {
        "name": "Patrick",
        "question": "Why would it?",
        "category": expect,
        "answer": "Why would it not?",
    } == result


@pytest.mark.django_db
def test_answer_update_post_save_not_publish(client):
    q_and_a = QandAFactory(answer="")
    assert QandA.QANDA_STATE_ASKED == q_and_a.status
    assert 1 == QandA.objects.needing_answers().count()
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("qanda.answer", args=[q_and_a.pk])
    response = client.post(url, data={"answer": "Why would it not?"})
    assert HTTPStatus.FOUND == response.status_code
    assert reverse("qanda.list.unanswered") == response.url
    # not published - so still needs an answer
    assert 1 == QandA.objects.needing_answers().count()
    q_and_a.refresh_from_db()
    assert "Why would it not?" == q_and_a.answer
    assert user == q_and_a.mentor
    assert QandA.QANDA_STATE_CLAIMED == q_and_a.status


# @pytest.mark.django_db
# def test_mentor_create(client):
#     user = UserFactory(is_staff=True)
#     assert client.login(username=user.username, password=TEST_PASSWORD) is True
#     assert 0 == QandAMentor.objects.count()
#     response = client.post(
#         reverse("qanda.mentor.create"), data={"email": "patrick@pkimber.net"}
#     )
#     assert HTTPStatus.FOUND == response.status_code
#     assert reverse("qanda.mentor.list") == response.url
#     assert 1 == QandAMentor.objects.count()
#     x = QandAMentor.objects.first()
#     assert "patrick@pkimber.net" == x.email
#     assert x.deleted is False
#
#
# @pytest.mark.django_db
# def test_mentor_delete(client):
#     user = UserFactory(is_staff=True)
#     assert client.login(username=user.username, password=TEST_PASSWORD) is True
#     x = QandAMentorFactory(email="patrick@pkimber.net")
#     assert x.deleted is False
#     response = client.post(reverse("qanda.mentor.delete", args=[x.pk]))
#     assert HTTPStatus.FOUND == response.status_code
#     assert reverse("qanda.mentor.list") == response.url
#     assert 1 == QandAMentor.objects.count()
#     x.refresh_from_db()
#     assert x.deleted is True
#     assert user == x.user_deleted
#     assert date.today() == x.date_deleted.date()
#     assert "patrick@pkimber.net" == x.email
#
#
# @pytest.mark.django_db
# def test_mentor_list(client):
#     user = UserFactory(is_staff=True)
#     assert client.login(username=user.username, password=TEST_PASSWORD) is True
#     x = QandAMentorFactory(email="patrick@pkimber.net")
#     x.set_deleted(user)
#     QandAMentorFactory(email="web@pkimber.net")
#     response = client.get(reverse("qanda.mentor.list"))
#     assert HTTPStatus.OK == response.status_code
#     assert "object_list" in response.context
#     assert ["web@pkimber.net"] == [
#         x.email for x in response.context["object_list"]
#     ]


@pytest.mark.django_db
def test_qanda_list_unanswered(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    q1 = QandAFactory(question="q1", answer="", status=QandA.QANDA_STATE_ASKED)
    q2 = QandAFactory(
        question="q2", answer="", status=QandA.QANDA_STATE_CLAIMED
    )
    q3 = QandAFactory(
        question="q3", answer="my answer", status=QandA.QANDA_STATE_ASKED
    )
    q4 = QandAFactory(
        question="q4", answer="", status=QandA.QANDA_STATE_PUBLISHED
    )
    response = client.get(reverse("qanda.list.unanswered"))
    assert HTTPStatus.OK == response.status_code
    assert "object_list" in response.context
    assert ["q3", "q2", "q1"] == [
        x.question for x in response.context["object_list"]
    ]
