# -*- encoding: utf-8 -*-
from django.apps import apps as django_apps
from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from django.db import models
from django.db.models import Q
from reversion import revisions as reversion

from base.model_utils import TimeStampedModel


class QandAError(Exception):
    def __init__(self, value):
        Exception.__init__(self)
        self.value = value

    def __str__(self):
        return repr("{}, {}".format(self.__class__.__name__, self.value))


def get_category_model():
    """
    Return the User model that is active in this project.
    """
    try:
        return django_apps.get_model(
            settings.QANDA_CATEGORY_MODEL, require_ready=False
        )
    except ValueError:
        raise ImproperlyConfigured(
            "QANDA_CATEGORY_MODEL must be of the form 'app_label.model_name'"
        )
    except LookupError:
        raise ImproperlyConfigured(
            "QANDA_CATEGORY_MODEL refers to model '{}' that has "
            "not been installed".format(settings.QANDA_CATEGORY_MODEL)  # noqa
        )


def get_contact_model():
    return django_apps.get_model(settings.CONTACT_MODEL)


def is_staff_or_team_member(user):
    """Copied from 'exam/models.py'."""
    result = user.is_staff
    if not result:
        result = is_team_member(user)
    return result


def is_team_member_not_staff(user):
    """Is this a team member (but not a member of staff).

    Copied from 'exam/models.py'.

    """
    result = False
    if user.is_staff:
        pass
    else:
        result = is_team_member(user)
    return result


def is_team_member(user):
    """Copied from 'exam/models.py'."""
    result = False
    if user.is_anonymous:
        pass
    else:
        contact_model = get_contact_model()
        try:
            contact = contact_model.objects.get(user=user)
            result = contact.is_team_member()
        except contact_model.DoesNotExist:
            pass
    return result


class QandAManager(models.Manager):
    def ask(self, user, question, category=None):
        x = self.model(
            user=user, question=question, status=QandA.QANDA_STATE_ASKED
        )
        if category:
            x.category = category
        x.save()
        return x

    def questions_and_answers(self, user=None, category=None):
        """List of all questions with answers."""
        qandas = self.model.objects.all()
        if user:
            qandas = qandas.filter(Q(user=user) | Q(mentor=user))
        if category:
            qandas = qandas.filter(category=category)
        return qandas

    def needing_answers(self, user=None, category=None):
        """List of all questions which have not been answered."""
        qandas = self.model.objects.exclude(
            status__in=(
                QandA.QANDA_STATE_PUBLISHED,
                QandA.QANDA_STATE_MARK_COMPLETE,
            )
        )
        if user:
            qandas = qandas.filter(Q(user=user) | Q(mentor=user))
        if category:
            qandas = qandas.filter(category=category)
        return qandas

    def published(self, user=None, category=None):
        """List of all questions which have been published."""
        qandas = self.model.objects.filter(status=QandA.QANDA_STATE_PUBLISHED)
        if user:
            qandas = qandas.filter(Q(user=user) | Q(mentor=user))
        if category:
            qandas = qandas.filter(category=category)
        return qandas


@reversion.register()
class QandA(TimeStampedModel):
    """List of questions with an answer."""

    QANDA_STATE_ASKED = 0
    QANDA_STATE_CLAIMED = 1
    QANDA_STATE_PUBLISHED = 2
    QANDA_STATE_MARK_COMPLETE = 3
    QANDA_STATES = (
        (QANDA_STATE_ASKED, "Asked"),
        (QANDA_STATE_CLAIMED, "Claimed"),
        (QANDA_STATE_PUBLISHED, "Published"),
        (QANDA_STATE_MARK_COMPLETE, "Mark Complete"),
    )
    MAIL_TEMPLATE_QANDA_PUBLISH = "qanda_answer_publish"

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, related_name="+", on_delete=models.CASCADE
    )
    question = models.TextField()
    category = models.ForeignKey(
        settings.QANDA_CATEGORY_MODEL,
        blank=True,
        null=True,
        on_delete=models.CASCADE,
    )
    mentor = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="+",
        blank=True,
        null=True,
        on_delete=models.CASCADE,
    )
    answer = models.TextField(blank=True)
    status = models.IntegerField(
        default=QANDA_STATE_ASKED, choices=QANDA_STATES
    )
    reason = models.CharField(
        max_length=100,
        blank=True,
        help_text="Reason for not answering the question",
    )
    objects = QandAManager()

    class Meta:
        ordering = ("-created",)
        verbose_name = "QandA"
        verbose_name_plural = "QandAs"

    def __str__(self):
        return "{}".format(self.question)

    def answer_question(self, mentor, answer, status):
        self.mentor = mentor
        self.answer = answer
        self.status = status
        self.save()

    def button_caption(self):
        result = ""
        if self.status == self.QANDA_STATE_ASKED:
            result = "Review"
        elif self.status == self.QANDA_STATE_CLAIMED:
            if self.answer:
                result = "Draft"
            else:
                result = "Review"
        elif self.status == self.QANDA_STATE_MARK_COMPLETE:
            result = "Completed"
        elif self.status == self.QANDA_STATE_PUBLISHED:
            result = "Published"
        return result

    def can_mark_complete(self):
        return self.status in (self.QANDA_STATE_ASKED, self.QANDA_STATE_CLAIMED)

    def is_completed(self):
        return self.status == self.QANDA_STATE_MARK_COMPLETE


# class QandAMentorManager(models.Manager):
#     def current(self):
#         return self.model.objects.exclude(deleted=True)
#
#
# @reversion.register()
# class QandAMentor(TimedCreateModifyDeleteModel):
#     """List of people to notify when a question is posted."""
#
#     email = models.EmailField()
#     objects = QandAMentorManager()
#
#     class Meta:
#         verbose_name = "QandA Mentor"
#         verbose_name_plural = "QandA Mentors"
#
#     def __str__(self):
#         return "{}".format(self.email)
