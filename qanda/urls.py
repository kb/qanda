# -*- encoding: utf-8 -*-
from django.urls import re_path

from .views import (
    AnswerUpdateView,
    MarkCompleteUpdateView,
    QandAListView,
    # QandAMentorCreateView,
    # QandAMentorDeleteView,
    # QandAMentorListView,
    QandAUnansweredListView,
)


urlpatterns = [
    re_path(
        r"^(?P<pk>\d+)/answer/$",
        view=AnswerUpdateView.as_view(),
        name="qanda.answer",
    ),
    re_path(
        r"^(?P<pk>\d+)/mark/complete/$",
        view=MarkCompleteUpdateView.as_view(),
        name="qanda.mark.complete",
    ),
    # re_path(
    #     r"^mentor/$",
    #     view=QandAMentorListView.as_view(),
    #     name="qanda.mentor.list",
    # ),
    # re_path(
    #     r"^mentor/create/$",
    #     view=QandAMentorCreateView.as_view(),
    #     name="qanda.mentor.create",
    # ),
    # re_path(
    #     r"^mentor/(?P<pk>\d+)/delete/$",
    #     view=QandAMentorDeleteView.as_view(),
    #     name="qanda.mentor.delete",
    # ),
    re_path(
        r"^unanswered/$",
        view=QandAUnansweredListView.as_view(),
        name="qanda.list.unanswered",
    ),
    re_path(r"", view=QandAListView.as_view(), name="qanda.list"),
]
