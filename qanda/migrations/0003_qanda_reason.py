# Generated by Django 4.2.4 on 2024-07-26 10:12

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("qanda", "0002_alter_qanda_answer_alter_qanda_category_and_more"),
    ]

    operations = [
        migrations.AddField(
            model_name="qanda",
            name="reason",
            field=models.CharField(
                blank=True,
                help_text="Reason for not answering the question",
                max_length=100,
            ),
        ),
    ]
