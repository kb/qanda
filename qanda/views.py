# -*- encoding: utf-8 -*-
import humanize
import logging

from braces.views import LoginRequiredMixin, UserPassesTestMixin
from django.contrib import messages
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.core.exceptions import PermissionDenied
from django.db import transaction
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.utils import timezone
from django.views.generic import ListView, UpdateView

from base.view_utils import BaseMixin, RedirectNextMixin
from mail.service import queue_mail_template
from mail.tasks import process_mail
from .forms import AnswerForm, MarkCompleteForm, QuestionForm
from .models import (
    get_category_model,
    QandA,
    is_staff_or_team_member,
    is_team_member_not_staff,
)


logger = logging.getLogger(__name__)


class AnswerUpdateView(
    LoginRequiredMixin,
    UserPassesTestMixin,
    RedirectNextMixin,
    BaseMixin,
    UpdateView,
):
    """Answer a question...

    Template is ``qanda/qanda_form.html``

    """

    form_class = AnswerForm
    model = QandA

    def _send_email_to_user(self):
        context = dict(
            answer=self.object.answer,
            name=self.object.user.first_name.capitalize(),
            question=self.object.question,
        )
        if self.object.category:
            context.update({"category": self.object.category.name})
        else:
            context.update({"category": "General Query Answer"})
        queue_mail_template(
            self.object,
            QandA.MAIL_TEMPLATE_QANDA_PUBLISH,
            {self.object.user.email: context},
        )
        messages.info(
            self.request,
            f"Answer published.  Sent an email to '{self.object.user.email}'",
        )

    def form_valid(self, form):
        self.object = form.save(commit=False)
        with transaction.atomic():
            if "publish" in self.request.POST:
                self.object.mentor = self.request.user
                self.object.status = QandA.QANDA_STATE_PUBLISHED
                self._send_email_to_user()
                transaction.on_commit(lambda: process_mail.send())
            result = super().form_valid(form)
        return result

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        warning = None
        if self.object.mentor:
            if self.request.user == self.object.mentor:
                pass
            else:
                warning = "{}: Question claimed by {} ({} ago)".format(
                    self.object.button_caption(),
                    self.object.mentor.get_full_name(),
                    humanize.naturaldelta(
                        timezone.now() - self.object.modified
                    ),
                )
        context.update(dict(warning=warning))
        return context

    def get_object(self, queryset=None):
        obj = super().get_object(queryset)
        if obj.mentor:
            pass
        else:
            obj.mentor = self.request.user
            obj.status = QandA.QANDA_STATE_CLAIMED
            obj.save()
        return obj

    def get_success_url(self):
        next_url = self.request.POST.get(REDIRECT_FIELD_NAME)
        if next_url:
            return next_url
        else:
            return reverse("qanda.list.unanswered")

    def test_func(self, user):
        return is_staff_or_team_member(user)


class MarkCompleteUpdateView(
    LoginRequiredMixin,
    UserPassesTestMixin,
    RedirectNextMixin,
    BaseMixin,
    UpdateView,
):
    form_class = MarkCompleteForm
    model = QandA
    template_name = "qanda/qanda_mark_complete_form.html"

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.mentor = self.request.user
        self.object.status = QandA.QANDA_STATE_MARK_COMPLETE
        return super().form_valid(form)

    def get_success_url(self):
        next_url = self.request.POST.get(REDIRECT_FIELD_NAME)
        if next_url:
            return next_url
        else:
            return reverse("qanda.list.unanswered")

    def test_func(self, user):
        return is_staff_or_team_member(user)


class QandADetailMixin:
    """Display the question (and answer)

    A question (and answer) can be viewed by a member of staff or the user who
    created the question.

    """

    model = QandA

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.user.is_staff or self.object.user == self.request.user:
            pass
        else:
            raise PermissionDenied(
                "A user can only view their own questions "
                "('{}' != '{}' for question {})".format(
                    self.request.user, self.object.user, self.object.pk
                )
            )
        return context


class QandAListView(
    LoginRequiredMixin, UserPassesTestMixin, BaseMixin, ListView
):
    model = QandA
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(
            dict(
                is_team_member_not_staff=is_team_member_not_staff(
                    self.request.user
                ),
                unanswered=False,
            )
        )
        return context

    def test_func(self, user):
        return is_staff_or_team_member(user)


# class QandAMentorCreateView(
#     LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, CreateView
# ):
#
#     form_class = QandAMentorForm
#     model = QandAMentor
#
#     def get_success_url(self):
#         return reverse("qanda.mentor.list")
#
#
# class QandAMentorDeleteView(
#     LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
# ):
#
#     form_class = QandAMentorEmptyForm
#     template_name = "qanda/mentor_delete_form.html"
#     model = QandAMentor
#
#     def form_valid(self, form):
#         self.object = form.save(commit=False)
#         self.object.set_deleted(self.request.user)
#         return HttpResponseRedirect(self.get_success_url())
#
#     def get_success_url(self):
#         return reverse("qanda.mentor.list")
#
#
# class QandAMentorListView(
#     LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
# ):
#
#     model = QandAMentor
#
#     def get_queryset(self):
#         return QandAMentor.objects.current()


class QandAUnansweredListView(
    LoginRequiredMixin, UserPassesTestMixin, BaseMixin, ListView
):
    paginate_by = 20

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(
            dict(
                is_team_member_not_staff=is_team_member_not_staff(
                    self.request.user
                ),
                unanswered=True,
            )
        )
        return context

    def get_queryset(self):
        return QandA.objects.needing_answers()

    def test_func(self, user):
        return is_staff_or_team_member(user)


class QuestionCreateMixin:
    """Create a question (without an answer)."""

    form_class = QuestionForm
    model = QandA

    def _category(self):
        result = None
        category_pk = self.kwargs.get("category_pk")
        if category_pk:
            result = get_category_model().objects.get(pk=category_pk)
        return result

    def form_valid(self, form):
        self.object = form.save(commit=False)
        category = self._category()
        question = form.cleaned_data["question"]
        self.object = QandA.objects.ask(self.request.user, question, category)
        # with transaction.atomic():
        #     result = super().form_valid(form)
        #     # notify mentors
        #     email_addresses = [x.email for x in QandAMentor.objects.current()]
        #     if email_addresses:
        #         message = (
        #             "LMS question #{} from {}:\n\n{}\n\nTo answer, {}".format(
        #                 self.object.pk,
        #                 self.object.user.get_full_name(),
        #                 self.object.question,
        #                 urllib.parse.urljoin(
        #                     settings.HOST_NAME,
        #                     reverse("qanda.answer", args=[self.object.pk]),
        #                 ),
        #             )
        #         )
        #         queue_mail_message(
        #             self.object,
        #             email_addresses,
        #             "LMS question #{}".format(self.object.pk),
        #             message,
        #         )
        #     else:
        #         logging.error(
        #             "Cannot send email notification of question.  "
        #             "No email addresses set-up in 'qanda.models.QandAMentor'"
        #         )
        #     transaction.on_commit(lambda: process_mail.send())
        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(dict(category=self._category()))
        return context
