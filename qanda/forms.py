# -*- encoding: utf-8 -*-
from django import forms

from base.form_utils import RequiredFieldForm
from .models import QandA


class AnswerForm(RequiredFieldForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        f = self.fields["answer"]
        f.widget.attrs.update({"class": "pure-input-1", "rows": 15})

    class Meta:
        model = QandA
        fields = ("answer",)

    def clean_answer(self):
        data = self.cleaned_data.get("answer")
        if not data.strip():
            raise forms.ValidationError(
                "You must enter an answer",
                code="qanda__answer__empty",
            )
        return data


class MarkCompleteForm(RequiredFieldForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        f = self.fields["reason"]
        f.widget.attrs.update({"class": "pure-input-2-3"})

    class Meta:
        model = QandA
        fields = ("reason",)

    def clean_reason(self):
        data = self.cleaned_data.get("reason")
        if not data.strip():
            raise forms.ValidationError(
                "You must enter a reason",
                code="qanda__reason__empty",
            )
        return data


# class QandAMentorEmptyForm(forms.ModelForm):
#    class Meta:
#        model = QandAMentor
#        fields = ()
#
#
# class QandAMentorForm(RequiredFieldForm):
#    def __init__(self, *args, **kwargs):
#        super().__init__(*args, **kwargs)
#        for name in ("email",):
#            self.fields[name].widget.attrs.update({"class": "pure-input-2-3"})
#
#    class Meta:
#        model = QandAMentor
#        fields = ("email",)


class QuestionForm(RequiredFieldForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["question"].widget.attrs.update(
            {
                "class": "pure-input-1",
                "cols": "80",
                "style": "width:100%",
            }
        )

    class Meta:
        model = QandA
        fields = ("question",)
