# -*- encoding: utf-8 -*-
from django.conf import settings
from django.core.management.base import BaseCommand

from mail.models import MailTemplate
from qanda.models import QandA


class Command(BaseCommand):
    help = "Initialise 'qanda' application"

    def handle(self, *args, **options):
        self.stdout.write(f"{self.help}")
        mail_template = MailTemplate.objects.init_mail_template(
            QandA.MAIL_TEMPLATE_QANDA_PUBLISH,
            "Q&A - answer pubished",
            (
                "You can add the following variables to the template:\n"
                "{{ name }} name of the person.\n"
                "{{ category }}.\n"
                "{{ question }}.\n"
                "{{ answer }}.\n"
            ),
            False,
            settings.MAIL_TEMPLATE_TYPE,
            is_system=True,
        )
        self.stdout.write(
            f"Initialised mail template: {mail_template.title} '{mail_template.slug}'"
        )
        self.stdout.write(f"{self.help} - Complete")
