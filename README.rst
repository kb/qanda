QandA
*****

Django application for question and answer

Install
=======

Virtual Environment
-------------------

::

  virtualenv --python=python3 venv-qanda
  # or...
  python3 -m venv venv-qanda

  source venv-qanda/bin/activate
  pip install -r requirements/local.txt

Testing
=======

::

  find . -name '*.pyc' -delete
  py.test -x

Usage
=====

::

  ./init_dev.sh

Release
=======

https://www.kbsoftware.co.uk/docs/
