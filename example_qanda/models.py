# -*- encoding: utf-8 -*-
from django.conf import settings
from django.db import models


class Contact(models.Model):
    """Copied from 'example_exam'."""

    user = models.OneToOneField(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE
    )
    team_member = models.BooleanField(default=False)

    class Meta:
        ordering = ("user__username",)
        verbose_name = "Contact"

    def email(self):
        return self.user.email

    def full_name(self):
        return f"{self.user.first_name} {self.user.last_name}"

    def is_team_member(self):
        """Is this contact a team member?"""
        return self.team_member


class QandACategoryManager(models.Manager):
    def create_qanda_category(self, name):
        obj = self.model(name=name)
        obj.save()
        return obj

    def init_qanda_category(self, name):
        try:
            obj = self.model.objects.get(name=name)
            obj.save()
        except self.model.DoesNotExist:
            obj = self.create_qanda_category(name)
        return obj


class QandACategory(models.Model):
    name = models.CharField(max_length=100)
    objects = QandACategoryManager()

    def __str__(self):
        return "{}".format(self.name)
