# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from example_qanda.tests.scenario import default_scenario_qanda


class Command(BaseCommand):
    help = "Create demo data for 'product'"

    def handle(self, *args, **options):
        default_scenario_qanda()
        print("Created 'Q&A' demo data...")
