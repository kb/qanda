# -*- encoding: utf-8 -*-
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import include, re_path

from .views import (
    DashView,
    HomeView,
    LmsDashView,
    QandADetailView,
    QuestionCreateView,
    SettingsView,
    UserContactRedirectView,
)


urlpatterns = [
    re_path(r"^$", view=HomeView.as_view(), name="project.home"),
    re_path(
        r"^settings/$",
        view=SettingsView.as_view(),
        name="project.settings",
    ),
    re_path(r"^", view=include("login.urls")),
    re_path(r"^$", view=HomeView.as_view(), name="project.home"),
    re_path(r"^dash/$", view=DashView.as_view(), name="project.dash"),
    re_path(r"^lms/dash/$", view=LmsDashView.as_view(), name="lms.dash"),
    re_path(r"^qanda/", view=include("qanda.urls")),
    re_path(
        r"^example/qanda/(?P<pk>\d+)/$",
        view=QandADetailView.as_view(),
        name="example.qanda.detail",
    ),
    re_path(
        r"^example/question/create/$",
        view=QuestionCreateView.as_view(),
        name="example.question.create",
    ),
    re_path(
        r"^example/category/(?P<category_pk>\d+)/question/create/$",
        view=QuestionCreateView.as_view(),
        name="example.category.question.create",
    ),
    re_path(
        r"^user/(?P<pk>\d+)/redirect/$",
        view=UserContactRedirectView.as_view(),
        name="user.redirect.contact",
    ),
]


urlpatterns += staticfiles_urlpatterns()

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
        re_path(r"^__debug__/", include(debug_toolbar.urls))
    ] + urlpatterns
