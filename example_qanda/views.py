# -*- encoding: utf-8 -*-
from braces.views import (
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    UserPassesTestMixin,
)
from django.urls import reverse
from django.views.generic import (
    CreateView,
    DetailView,
    RedirectView,
    TemplateView,
)

from base.view_utils import BaseMixin
from qanda.models import get_category_model, QandA
from qanda.views import QandADetailMixin, QuestionCreateMixin


class DashView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, TemplateView
):
    model = QandA
    template_name = "example/dash.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(
            dict(categories=get_category_model().objects.all().order_by("name"))
        )
        return context


class HomeView(BaseMixin, TemplateView):
    template_name = "example/home.html"


class LmsDashView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    BaseMixin,
    TemplateView,
):
    """LMS is designed to *sit above* the 'exam' and 'qanda' apps."""

    template_name = "example/lms-dash.html"


class QandADetailView(
    LoginRequiredMixin,
    QandADetailMixin,
    BaseMixin,
    DetailView,
):
    pass


class QuestionCreateView(
    LoginRequiredMixin,
    QuestionCreateMixin,
    BaseMixin,
    CreateView,
):
    def get_success_url(self):
        return reverse("example.qanda.detail", args=[self.object.pk])


class SettingsView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, TemplateView
):
    template_name = "example/settings.html"


class UserContactRedirectView(RedirectView):
    permanent = False

    def get_redirect_url(self, *args, **kwargs):
        pk = kwargs["pk"]
        contact_model = get_contact_model()
        contact = contact_model.objects.get(user__pk=pk)
        return reverse("contact.detail", args=[contact.pk])
