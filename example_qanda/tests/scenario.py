# -*- encoding: utf-8 -*-
from login.tests.scenario import get_user_fred, get_user_mike, get_user_sara
from qanda.models import QandA, get_category_model


def default_scenario_qanda():
    cat1 = get_category_model().objects.init_qanda_category(name="General")
    cat2 = get_category_model().objects.init_qanda_category(name="Other")
    u1 = get_user_fred()
    u2 = get_user_mike()
    u3 = get_user_sara()
    q1 = QandA.objects.ask(u1, "What is blue?", cat1)
    q2 = QandA.objects.ask(u1, "What is green?", cat1)
    QandA.objects.ask(u2, "What is red?", cat2)
    q1.answer_question(
        u2, "Very little green and red.", QandA.QANDA_STATE_CLAIMED
    )
    q2.answer_question(
        u2, "Very little blue and red.", QandA.QANDA_STATE_PUBLISHED
    )
    q4 = QandA.objects.ask(u3, "What are insects?", cat1)
    QandA.objects.ask(u3, "What are mammals?", cat2)
    QandA.objects.ask(u3, "What are reptiles?", cat1)
    q4.answer_question(
        u1, "Very annoying, unless they eat insects.", QandA.QANDA_STATE_CLAIMED
    )
