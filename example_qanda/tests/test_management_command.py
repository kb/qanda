# -*- encoding: utf-8 -*-
import pytest

from django.core.management import call_command


@pytest.mark.django_db
def test_demo_data():
    """Test the management commands."""
    call_command("demo-data-login")
    call_command("init_app_qanda")
    call_command("demo_data_qanda")
