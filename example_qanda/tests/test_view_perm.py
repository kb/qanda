# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse
from http import HTTPStatus

from login.tests.factories import TEST_PASSWORD, UserFactory
from login.tests.fixture import perm_check
from qanda.tests.factories import QandACategoryFactory, QandAFactory


@pytest.mark.django_db
def test_lms_dash(perm_check):
    url = reverse("lms.dash")
    perm_check.staff(url)


@pytest.mark.django_db
def test_qanda_detail_staff(perm_check):
    x = QandAFactory()
    url = reverse("example.qanda.detail", args=[x.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_qanda_detail_user(client):
    user = UserFactory(username="patrick")
    x = QandAFactory(user=user)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(reverse("example.qanda.detail", args=[x.pk]))
    assert HTTPStatus.OK == response.status_code


@pytest.mark.django_db
def test_qanda_detail_user_diff(client):
    user = UserFactory(username="patrick")
    x = QandAFactory(user=UserFactory(username="another"))
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(reverse("example.qanda.detail", args=[x.pk]))
    assert HTTPStatus.FORBIDDEN == response.status_code


@pytest.mark.django_db
def test_question_create(perm_check):
    category = QandACategoryFactory()
    url = reverse("example.category.question.create", args=[category.pk])
    perm_check.auth(url)
