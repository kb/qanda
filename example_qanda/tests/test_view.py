# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse
from http import HTTPStatus

from login.tests.factories import TEST_PASSWORD, UserFactory
from mail.models import Message
from qanda.models import QandA
from qanda.tests.factories import QandACategoryFactory


@pytest.mark.django_db
def test_question_create(client):
    # QandAMentorFactory(email="web@pkimber.net")
    assert 0 == QandA.objects.count()
    user = UserFactory(first_name="B", last_name="Primrose")
    category = QandACategoryFactory()
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("example.category.question.create", args=[category.pk])
    response = client.post(url, data={"question": "Did this test work?"})
    assert HTTPStatus.FOUND == response.status_code
    assert 1 == QandA.objects.count()
    qanda = QandA.objects.first()
    assert reverse("example.qanda.detail", args=[qanda.pk]) == response.url
    assert user == qanda.user
    assert "Did this test work?" == qanda.question
    assert category == qanda.category
    assert qanda.mentor is None
    assert "" == qanda.answer
    assert QandA.QANDA_STATE_ASKED == qanda.status
    # check email was sent to mentor
    # assert 1 == Message.objects.count()
    # message = Message.objects.first()
    # assert 1 == message.mail_set.count()
    # assert (
    #     "LMS question #{} from B Primrose:".format(qanda.pk)
    #     in message.description
    # )
    # assert "Did this test work?" in message.description
    # assert "To answer" in message.description
    # assert reverse("qanda.answer", args=[qanda.pk]) in message.description
    # mail = message.mail_set.first()
    # assert "web@pkimber.net" == mail.email


@pytest.mark.django_db
def test_question_create_no_category(client):
    # QandAMentorFactory(email="web@pkimber.net")
    assert 0 == QandA.objects.count()
    user = UserFactory(first_name="B", last_name="Primrose")
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("example.question.create")
    response = client.post(url, data={"question": "Favourite colour?"})
    assert HTTPStatus.FOUND == response.status_code
    assert 1 == QandA.objects.count()
    qanda = QandA.objects.first()
    assert reverse("example.qanda.detail", args=[qanda.pk]) == response.url
    assert user == qanda.user
    assert "Favourite colour?" == qanda.question
    assert qanda.category is None
    assert qanda.mentor is None
    assert "" == qanda.answer
    assert QandA.QANDA_STATE_ASKED == qanda.status
